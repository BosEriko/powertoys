# PowerToys (Hotkeys)
Download [Scoop](https://github.com/BosEriko/scoop) as your package manager then install [PowerToys](https://scoop.sh/#/apps?q=powertoys).

## Install Visual Studio Code
```sh
scoop bucket add extras
scoop install extras/powertoys
```

## Update Keyboard Manager
Enable and update your keybindings with this [configuration](keybindings.md).

## Update PowerToys Run
Enable and update PowerToys Run with `Search delay (ms)`.